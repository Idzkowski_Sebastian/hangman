$(document).ready(function(){
	
	alphabet(); 

	$('#buttonStart').click(function(){
		var passwordStart = $("input[name=inputPass]").val();
		haslo = passwordStart; 
		haslo=haslo.toUpperCase();
		print_pas(haslo);
		$('#passwordInput').hide();
		$('#board').fadeIn('slow');	
		start.play();
	})

	$('.character').click(function() {
		var ch_this = $(this).html();
		var hasloChar;
		var flag=false; 
		var indeksChr;
		
		//pętla szukająca danej literki w haśle
		for (var i = 0; i < haslo.length; i++) {
		 	hasloChar = getChar(i);
			if( hasloChar === ch_this){
		 		flag = true;
		 		password1 = password1.replaceChar(i,ch_this); //funkcja zmieniająca - w literkę znalezioną w haśle
		 	}
		}

		 if (flag) { // jesli flaga == true wypisuje password z zamienionym'-' na literke, ustawia zielone literki oraz je wylacza 
		 	yes.play();
		 	$('#board').text(password1);
		 	$(this).addClass('characterGood'); 
			$(this).addClass('disabled');
		 
		 }else {
		 	no.play();
		 	count++; // licznik blednych klikniec, co bledne klikniecie dodaje nowy obrazek 
		 	$(this).addClass('characterBad').addClass('disabled'); 
		 	$('.stage').css("background-image", "url(img/s"+count+".jpg)"); 
		}
		if(count == 9){
		 	loose.play();
		 	$('.character').addClass('characterBad').addClass('disabled'); 
		 	$('#board').text("Game Over!");
		 	$('#buttonTry').html('Try Again!').show();
		}
		 
		if(haslo == password1){
			win.play();
			var stage = $('.stage');
			$('.character').addClass('disabled');
			stage.fadeOut(1000,0, function(){
			stage.fadeIn("slow");
			$('.stage').css('background-image','url(img/victory.jpg)');
			});
			$('#buttonTry').html('Start!').show();
		}
	})

	$('#buttonTry').click(function(){
		location.reload();
	})

	$("input[name='inputPass']").keypress(function(){
		klik.play();
	})	
})

var count = 0;
var haslo;
var password1="";
var litery = new Array(35);

var yes = new Audio("yes.wav");
var no = new Audio("no.wav");
var start = new Audio("StartRound.wav");
var win = new Audio("win1.wav");
var loose = new Audio("loose.wav");
var klik = new Audio("klik.wav");

var chars =["A","Ą","B","C","Ć","D","E","Ę","F","G","H","I","J","K","L","Ł","M","N","Ń","O","Ó","P","Q","R","S","Ś","T","U","V","W","X","Y","Z","Ż","Ź"];

//wypisuje haslo na ekranie i zamienia je na  ----
function print_pas(pass) {
	
	for (var i = 0; i < pass.length; i++) {
		if(pass.charAt(i)==' ') password1 += " ";
		else password1 += '-';
	}
	$('#board').text(password1);
}

//wypisuje alfabet i co 7 liter zaczyna nowy wiersz *** (i+1) % 7 div clear both ****
function alphabet(){
	var inside_div ='';
	
	for (var i = 0; i <35; i++) {
		inside_div += '<div class= "character">'+chars[i]+'</div>';
		if( (i+1)%7==0 ) inside_div += '<div style="clear:both"></div>'
	}
	$('#alphabet').html(inside_div);
}

//funkcja zamieniająca znaki w password1
String.prototype.replaceChar = function(place, charact) {

	if(place > this.length-1) return this.toString();
	else return this.substr(0,place) + charact +this.substr(place+1);
};

function getChar(i){
	return haslo.charAt(i);
}
